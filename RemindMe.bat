@echo off
::echo %%~dp0 is "%~dp0"
::echo %%~dpnx0 is "%~dpnx0"
SETLOCAL ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION
IF ERRORLEVEL 1 ECHO Unable to enable extensions 
GOTO START
::GOTO TEST // rem Test of the doClean "function"

REM --------------------------------------------------------------------
REM -               Script for windows by Olivier Sailly               -
REM -                                                                  -
REM -   Launch a notification in the windows terminal with given %1    -
REM -          To know more about this, contact Olivier at:            -
REM -              Olivier [DOT] Sailly [AT] GMX [DOT] FR              -
REM --------------------------------------------------------------------

:START
IF [%1] EQU [] (
	GOTO V1_NULL
) ELSE (
	GOTO V1_NOTNULL
)

:V1_NOTNULL
title New Notification! 
color c
cls
echo.
echo Remember: %1
echo.
GOTO END

:V1_NULL
echo Notification not found.
set "tmpPath=%~dp0.Tmp\"
set setFolderRetryCounter=0
GOTO IS_TMPDIR_A_DIR

:IS_TMPDIR_A_DIR
IF NOT EXIST "%tmpPath%" (
  GOTO TMPDIR_NOT_A_DIR
) ELSE (
  GOTO TMPDIR_IS_A_DIR
)

:TMPDIR_IS_A_DIR
echo Test on "%tmpPath%" : OK.
GOTO V1_NOTNULL_TMPDIR_ISSET

:TMPDIR_NOT_A_DIR
echo Test on "%tmpPath%" : NOT FOUND.
GOTO V1_NOTNULL_TMPDIR_CREATE

:V1_NOTNULL_TMPDIR_CREATE
echo Trying to create "%tmpPath%"...
IF %setFolderRetryCounter% GEQ 3 (
	GOTO V1_NOTNULL_TMPDIR_CREATE_FAILED
) ELSE (
	mkdir "%tmpPath%"
	set /a setFolderRetryCounter=%setFolderRetryCounter%+1
	GOTO IS_TMPDIR_A_DIR
)

:V1_NOTNULL_TMPDIR_CREATE_FAILED
echo Error: Impossible to create "%tmpPath%" after retrying 3 times.
GOTO END

:V1_NOTNULL_TMPDIR_ISSET
schtasks /Delete /tn "Scheduled Reminder" /f
GOTO V1_NOTNULL_TMPDIR_ISSET_USER_ENTRIES

:V1_NOTNULL_TMPDIR_ISSET_USER_ENTRIES
set /p notif=Enter the Notification (without "):%=%
set /p tmpTime=Enter the Time (xx:yy format):%=%
IF DEFINED tmpTime ( 
	GOTO TMPTIME_IS_SET
) ELSE ( 
	GOTO UNKNOWN_ERROR
)

:TMPTIME_IS_SET
set "tmpT=%tmpTime:~1,1%"
IF ":"=="%tmpT%" ( GOTO TMPTIME_IS_SET_AND_NOK ) ELSE ( GOTO TMPTIME_IS_SET_AND_OK  )

:TMPTIME_IS_SET_AND_NOK
echo Wrong time given (xx:yy wanted). Given time is "%tmpTime%".
set "finalTime=0%tmpTime%"
echo New time is : "%finalTime%".
GOTO CONTINUE_V1NOTNULL_TMPDIRISSET_USERENTRIESDONE

:TMPTIME_IS_SET_AND_OK
set "finalTime=%tmpTime%"
GOTO CONTINUE_V1NOTNULL_TMPDIRISSET_USERENTRIESDONE

:CONTINUE_V1NOTNULL_TMPDIRISSET_USERENTRIESDONE
set "finalTime=%finalTime:_=%"
set "finalTime=%finalTime::=_%"
::echo finalTime="%finalTime%"
call :doClean "%finalTime%" output "1234567890_"
set "finalTime=%output%"
::echo finalTime="%finalTime%"
call :doClean "%finalTime%" output "_"
set "tmpFT=%output%"
set "tmpFT=%tmpFT:~0,2%"
::echo tmpFT="%tmpFT%"
IF "__"=="%tmpFT%" ( GOTO WRONG_TIME_ERROR )
set "finalTime=%finalTime:_=:%"
::echo finalTime="%finalTime%"
::pause

::If ever we wanted to do all here without file generation, we may use this way :
::    > set notif=%notif:'=\'%
::    > echo %notif%
::    > pause

set setFileRetryCounter=0
set "fileName=%DATE:~6,4%%DATE:~3,2%%DATE:~0,2%-%TIME:~0,2%%TIME:~3,2%%TIME:~6,2%%TIME:~9,2%.bat"
set "fileName=%fileName: =0%"

:V1_NOTNULL_TMPDIR_ISSET_SETFILE
IF %setFileRetryCounter% GEQ 3 (
	GOTO V1_NOTNULL_TMPDIR_ISSET_SETFILE_FAILED
)

echo cmd /c %~dpnx0 "%notif%" > "%tmpPath%%fileName%"

IF NOT EXIST "%tmpPath%%fileName%" (
  set /a setFileRetryCounter=%setFileRetryCounter%+1
  GOTO V1_NOTNULL_TMPDIR_ISSET_SETFILE
)

echo Creating the task...
schtasks /Create /tn "Scheduled Reminder" /sc ONCE /st %finalTime% /tr "cmd /c \"%tmpPath%%fileName%\"" /it /f
IF ERRORLEVEL 1 (
	GOTO SCHTASKS_UNKNOWN_ERROR
)
echo All Done.
GOTO END

:WRONG_TIME_ERROR
echo Wrong schedule time value. Expected format: xx:yy 
GOTO END

:UNKNOWN_ERROR
echo Unexpected error.
GOTO END

:V1_NOTNULL_TMPDIR_ISSET_SETFILE_FAILED
echo Error: Impossible to create "%tmpPath%%fileName%" after retrying 3 times.
GOTO END

:SCHTASKS_UNKNOWN_ERROR
echo Task has not been scheduled. Please try retrying with different values.
GOTO END

















::---------------------------------------------------------------------------------------
::                                  PSEUDO FUNCTIONS
::---------------------------------------------------------------------------------------

:TEST
    rem Test empty string
    call :doClean "" output
    echo "%output%"
	
	rem Test mixed strings with custom map
	call :doClean "~~asd123#()%%%^"^!^"~~~^"""":^!!!!=asd^>^<bm_1" output "1234567890"
    echo "%output%"

    rem Test mixed strings
    call :doClean "~~asd123#()%%%^"^!^"~~~^"""":^!!!!=asd^>^<bm_1" output
    echo %output%
    call :doClean "Thi\s&& is ;;;;not ^^good _maybe_!~*???" output
    echo %output%

    rem Test clean string
    call :doClean "This is already clean" output
    echo %output%

    rem Test all bad string
    call :doClean "*******//////\\\\\\\()()()()" output
    echo "%output%"

    rem Test long string
    set "zz=Thi\s&& is not ^^good _maybe_!~*??? "
    set "zz=TEST: %zz%%zz%%zz%%zz%%zz%%zz%%zz%%zz%%zz%%zz%%zz%%zz%%zz%%zz%%zz%%zz%%zz%%zz%%zz%%zz%"
    call :doClean "%zz% TEST" output
    echo %output%

    rem Time long string
    echo %time%
    for /l %%# in (1 1 100) do call :doClean "%zz%" output
    echo %time%

    exit /b

rem ---------------------------------------------------------------------------
rem Clean a string using the valid keys in map
:doClean input output map
    setlocal enableextensions enabledelayedexpansion
	 
    set "input=%~1"
    set "output="
	IF ""=="%~3" ( set "map=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 " ) ELSE ( set "map=%~3" )

rem Step 1 - Remove critical delimiters
(
:purgeCritical
    for /L %%z in (1 1 10) do (
        for /f tokens^=1^-9^,^*^ delims^=^=^"^"^~^;^,^&^*^%%^:^!^(^)^<^>^^ %%a in ("!input!") do ( 
            set "output=!output!%%a%%b%%c%%d%%e%%f%%g%%h%%i"
            set "input=%%j" 
        )
        if not defined input goto outPurgeCritical
    )
    goto purgeCritical
)
:outPurgeCritical

rem Step 2 - remove any remaining special character
(
:purgeNormal
    for /L %%z in (1 1 10) do (
        set "pending="
        for /f "tokens=1,* delims=%map%" %%a in ("!output!") do (
            set "output=!output:%%a=!"
            set "pending=%%b"
        )
        if not defined pending goto outPurgeNormal
    )
    goto purgeNormal
)
:outPurgeNormal

    endlocal & set "%~2=%output%"
    goto :EOF
	
	
::--------------------------------------------------------------------------------------
:END
pause